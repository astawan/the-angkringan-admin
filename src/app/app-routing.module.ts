import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './_component/login/login.component';
import { DashboardComponent } from './_component/dashboard/dashboard.component';
import { AuthGuard } from './guards/auth/auth.guard';
import { LoginPageGuard } from './guards/login-page/login-page.guard';
import { UserComponent } from './_component/user/user.component';
import { SidenavComponent } from './_component/_sidenav/sidenav.component';
import { SidenavModule } from './_component/_sidenav/sidenav.module';
import { AuthService } from './_services/auth/auth.service';


const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full'},
  { 
    path: 'login',
    canActivate: [LoginPageGuard], 
    loadChildren: () =>
      import('./_component/login/login.module').then(
        m => m.LoginModule
      ) 
  },
  { 
    path: 'dashboard', 
    component: SidenavComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        loadChildren: () =>
          import('./_component/dashboard/dashboard.module').then(
            m => m.DashboardModule
          )

      },
      {
        path: 'member',
        loadChildren: () =>
          import('./_component/user/user.module').then(
            m => m.UserModule
          )
      },
      {
        path: 'category',
        loadChildren: () =>
          import('./_component/category/category.module').then(
            m => m.CategoryModule
          )
      },
      {
        path: 'recipe',
        loadChildren: () =>
          import('./_component/recipe/recipe.module').then(
            m => m.RecipeModule
          )
      },
      {
        path: 'slider',
        loadChildren: () =>
          import('./_component/slider/slider.module').then(
            m => m.SliderModule
          )
      }

    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    SidenavModule
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
