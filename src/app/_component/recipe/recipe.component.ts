import { Component, OnInit, ViewChild } from '@angular/core';
import { Recipe } from 'src/app/_interfaces/recipe';
import { MatMenuTrigger, MatPaginator, MatDialog, MatTableDataSource, MatDialogConfig } from '@angular/material';
import { ApiService } from 'src/app/_services/api/api.service';
import { HttpClient } from '@angular/common/http';
import { ViewRecipeComponent } from './dialog/view-recipe/view-recipe.component';

@Component({
  selector: 'app-recipe',
  templateUrl: './recipe.component.html',
  styleUrls: ['./recipe.component.scss']
})
export class RecipeComponent implements OnInit {
  displayedColumns = ["recipe", 'name', 'status'];
  dataRecipe: Recipe[] = [];
  dataSource: any;
  contextMenuPosition = { x: '0px', y: '0px' };

  selectedId: string;
  selectedRecipeName: string;
  selectedRecipeCategory: string;
  selectedDescription: string;
  selectedImage: string;
  selectedGallery: string;
  selectedVideo: string;
  selectedIngredient: string;
  selectedCookingMethod: string;
  selectedProvince: string;
  selectedCity: string;
  selectedStatus: string;
  selectedUserId: string;
  selectedUserName: string;
  
  @ViewChild(MatMenuTrigger, {static: false}) contextMenu: MatMenuTrigger;
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;

  constructor(
    private dialog: MatDialog,
    private apiService: ApiService,
    private http: HttpClient,
    ) { }

    ngOnInit() {
      this.subscribeRecipes();
    }
  
    subscribeRecipes() {
      this.apiService.getBaseData(this.apiService.getAllRecipesUrl).subscribe((res: Recipe[]) => {
        console.log(res);
        this.dataRecipe = res['data'];
        this.dataSource = new MatTableDataSource<Recipe>(this.dataRecipe);
        this.dataSource.paginator = this.paginator;
      }, err => {
      });
    }

    onContextMenu(event: MouseEvent, row) {
      event.preventDefault();
      console.log(row);
      this.selectedId = row.recipe_id;
      this.selectedRecipeName = row.recipe_name;
      this.selectedRecipeCategory = row.category_name;
      this.selectedDescription = row.description;
      this.selectedImage = row.image;
      this.selectedGallery = row.gallery;
      this.selectedVideo = row.video;
      this.selectedIngredient = row.inggridients;
      this.selectedCookingMethod = row.cookmethd;
      this.selectedProvince = row.province;
      this.selectedCity = row.city;
      this.selectedStatus = row.status;
      this.selectedUserId = row.user_id;
      this.selectedUserName = row.fullname;

      this.contextMenuPosition.x = event.clientX + 'px';
      this.contextMenuPosition.y = event.clientY + 'px';
      this.contextMenu.menu.focusFirstItem('mouse');
      this.contextMenu.openMenu();
    }

    viewCategory() {
      const dialogConfig = new MatDialogConfig();
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;
      dialogConfig.data = {
        id: this.selectedId,
        recipe_name: this.selectedRecipeName,
        recipe_category: this.selectedRecipeCategory,
        description: this.selectedDescription,
        image: this.selectedImage,
        gallery: this.selectedGallery,
        video: this.selectedVideo,
        inggridients: this.selectedIngredient,
        cookmethd: this.selectedCookingMethod,
        province: this.selectedProvince,
        city: this.selectedCity,
        status: this.selectedStatus,
        user_id: this.selectedUserId,
        fullname: this.selectedUserName,
      }
  
      let dialogRef = this.dialog.open(ViewRecipeComponent, dialogConfig);
  
      dialogRef.afterClosed().subscribe(result => {
        console.log(result);
        this.subscribeRecipes();
      });
    }

}
