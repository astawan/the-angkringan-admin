import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { ApiService } from 'src/app/_services/api/api.service';
import { AuthService } from 'src/app/_services/auth/auth.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AddCategoryComponent } from 'src/app/_component/category/dialog/add-category/add-category.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-view-recipe',
  templateUrl: './view-recipe.component.html',
  styleUrls: ['./view-recipe.component.scss']
})
export class ViewRecipeComponent implements OnInit {
  viewRecipeForm: FormGroup;
  selectedRecipe = this.data.recipe_name;
  status = [
    {
      value: 1,
      status_name: 'Waiting for review'
    },
    {
      value: 2,
      status_name: 'Accept'
    },
    {
      value: 3,
      status_name: 'Reject'
    },
  ]
  imgURL: any;
  constructor(
    private formBuilder: FormBuilder,
    private http: HttpClient,
    private apiService: ApiService,
    private authService: AuthService,
    private dialogRef:MatDialogRef<AddCategoryComponent>,
    private router: Router,
    @Inject(MAT_DIALOG_DATA) private data: any
  ) { }
  
  ngOnInit() {
    console.log(this.data);
    this.viewRecipeForm = this.formBuilder.group({
      recipe_name: [this.data.recipe_name],
      recipe_category: [this.data.recipe_category],
      description: [this.data.description],
      image: [this.data.image],
      video: [this.data.video],
      gallery: [this.data.gallery],
      inggridients: [this.data.inggridients],
      cookmethd: [this.data.cookmethd],
      province: [this.data.province],
      city: [this.data.city],
      status: [this.data.status],
      user_id: [this.data.user_id],
      fullname: [this.data.fullname],
    })
    if(this.data.image == null){
      this.imgURL = this.apiService.imagesno;
    }else{
      this.imgURL = this.data.image;
    }
  }

  close(){
    this.dialogRef.close();
  }

  saveRecipe(){
    console.log(this.data.id);
    let data = {
      'recipe_id' : this.data.id,
      'user_id' : localStorage.getItem('user_id'),
      'status' : this.f.status.value
    }
    let promise = new Promise((resolve, reject) => {
      let authorizedHeader = this.apiService.getAuthorizedHeader(localStorage.getItem('token'));
      this.http.post(this.apiService.changeRecipeStatusUrl,data,authorizedHeader)
        .toPromise()
        .then(
          res => { // Success
            console.log(res);
            if(res['status'] === true){
              this.dialogRef.close('Kategori berhasil diubah');
            }else{

            }
            resolve();
          }
        );
    })

    return promise;
  }

  get f() {
    return this.viewRecipeForm.controls;
  }

}
