import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RecipeRoutingModule } from './recipe-routing.module';
import { RecipeComponent } from './recipe.component';
import { AngularMaterialModule } from 'src/app/angular-material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ViewRecipeComponent } from './dialog/view-recipe/view-recipe.component';


@NgModule({
  declarations: [RecipeComponent, ViewRecipeComponent],
  imports: [
    CommonModule,
    RecipeRoutingModule,
    AngularMaterialModule,
    FormsModule,
    ReactiveFormsModule
  ],
  entryComponents: [ViewRecipeComponent]
})
export class RecipeModule { }
