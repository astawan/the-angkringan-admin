import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from 'src/app/_services/api/api.service';
import { MatDialogRef } from '@angular/material';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-add-slider',
  templateUrl: './add-slider.component.html',
  styleUrls: ['./add-slider.component.scss']
})
export class AddSliderComponent implements OnInit {
  addSliderForm: FormGroup;

  public imagePath;
  imgURL: any = this.apiService.imagesno;
  public message: string;
  fileToUpload: File = null;

  constructor(
    private apiService: ApiService,
    private formBuilder: FormBuilder,
    private dialogRef:MatDialogRef<AddSliderComponent>,
    private http: HttpClient
  ) { }

  ngOnInit() {
    this.addSliderForm = this.formBuilder.group({
      title: ['', Validators.required],
      subtitle: ['', Validators.required],
      image: ['', Validators.required],
      link: ['', Validators.required],
      url: ['', Validators.required]
    })
  }

  close(){
    this.dialogRef.close();
  }

  saveSlider(){
    const formData: FormData =  new FormData();
    formData.append('file', this.fileToUpload, this.fileToUpload.name);
    formData.append('user_id', localStorage.getItem('user_id'));
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'multipart/form-data');
    headers.append('Accept', 'application/json');
    let promise_upload = new Promise((resolve, reject) => {
      this.http.post(this.apiService.uploadSliderImageUrl,formData,{headers: headers})
        .toPromise()
        .then(
          res => { // Success
            console.log(res);
            if(res['status'] === true){
              let data = {
                'user_id' : localStorage.getItem('user_id'),
                'title' : this.f.title.value,
                'subtitle' : this.f.subtitle.value,
                'image' : `${this.apiService.upload_root}slider/${res['data'].image}`,
                'link' : this.f.link.value,
                'url' : this.f.url.value,
              }
              let promise = new Promise((resolve, reject) => {
                let authorizedHeader = this.apiService.getAuthorizedHeader(localStorage.getItem('token'));
                this.http.post(this.apiService.addSliderUrl,data,authorizedHeader)
                  .toPromise()
                  .then((res) => { // Success
                      console.log(res);
                      if(res['status'] === true){
                        this.dialogRef.close('Slider berhasil disimpan');
                      }
                      resolve();
                    }
                  )
              })
          
              return promise;
            }else{

            }
            resolve();
          }
        );

    })
    
    return promise_upload;
  }

  get f() {
    return this.addSliderForm.controls;
  }
 
  preview(files: FileList) {
    console.log(files);
    this.fileToUpload = files.item(0);
    if (files.length === 0)
      return;
 
    var mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.message += "Only images are supported.\n";
      return;
    }else{
      this.message = "";
    }

    if (files[0].size > 100000) {
      this.message += "File size must not exceed 100 KB.\n";
      return;
    }else{
      this.message = "";
    }
 
    var reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]); 
    reader.onload = (_event) => { 
      this.imgURL = reader.result; 
    }
  }

}
