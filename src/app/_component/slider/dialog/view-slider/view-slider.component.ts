import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from 'src/app/_services/api/api.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { UploadFileService } from 'src/app/_services/upload-file/upload-file.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-view-slider',
  templateUrl: './view-slider.component.html',
  styleUrls: ['./view-slider.component.scss']
})
export class ViewSliderComponent implements OnInit {
  editSliderForm: FormGroup;
  selectedSlider = this.data.title;

  public imagePath;
  imgURL: any;
  public message: string;
  fileToUpload: File = null;

  constructor(
    private formBuilder: FormBuilder,
    private http: HttpClient,
    private apiService: ApiService,
    private dialogRef:MatDialogRef<ViewSliderComponent>,
    @Inject(MAT_DIALOG_DATA) private data: any
  ) { }

  ngOnInit() {
    console.log(this.data);
    this.editSliderForm = this.formBuilder.group({
      title: [this.data.title, Validators.required],
      subtitle: [this.data.subtitle, Validators.required],
      image: [this.data.image, Validators.required],
      link: [this.data.link, Validators.required],
      url: [this.data.url, Validators.required],
    })
    if(this.data.image == null){
      this.imgURL = this.apiService.imagesno;
    }else{
      this.imgURL = this.data.image;
    }
  }

  close(){
    this.dialogRef.close();
  }

  saveEditSlider(){
    if(this.fileToUpload){
      const formData: FormData =  new FormData();
      formData.append('file', this.fileToUpload, this.fileToUpload.name);
      formData.append('user_id', localStorage.getItem('user_id'));
      const headers = new HttpHeaders();
      headers.append('Content-Type', 'multipart/form-data');
      headers.append('Accept', 'application/json');
      let promise_upload = new Promise((resolve, reject) => {
        this.http.post(this.apiService.uploadSliderImageUrl,formData,{headers: headers})
          .toPromise()
          .then(
            res => { // Success
              console.log(res);
              if(res['status'] === true){
                let data = {
                  'user_id' : localStorage.getItem('user_id'),
                  'id' : this.data.id,
                  'title' : this.f.title.value,
                  'subtitle' : this.f.subtitle.value,
                  'image' : `${this.apiService.upload_root}slider/${res['data'].image}`,
                  'link' : this.f.link.value,
                  'url' : this.f.url.value
                }
                let promise = new Promise((resolve, reject) => {
                  let authorizedHeader = this.apiService.getAuthorizedHeader(localStorage.getItem('token'));
                  this.http.post(this.apiService.saveEditSliderUrl,data,authorizedHeader)
                    .toPromise()
                    .then(
                      res => { // Success
                        console.log(res);
                        if(res['status'] === true){
                          this.dialogRef.close('Slider berhasil diubah');
                        }else{
            
                        }
                        resolve();
                      }
                    );
                })
            
                return promise;
              }else{
  
              }
              resolve();
            }
          );
  
      })
      
      return promise_upload;

    }else{
      let data = {
        'user_id' : localStorage.getItem('user_id'),
        'id' : this.data.id,
        'title' : this.f.title.value,
        'subtitle' : this.f.subtitle.value,
        'image' : this.imgURL,
        'link' : this.f.link.value,
        'url' : this.f.url.value
      }
      let promise = new Promise((resolve, reject) => {
        let authorizedHeader = this.apiService.getAuthorizedHeader(localStorage.getItem('token'));
        this.http.post(this.apiService.saveEditSliderUrl,data,authorizedHeader)
          .toPromise()
          .then(
            res => { // Success
              console.log(res);
              if(res['status'] === true){
                this.dialogRef.close('Slider berhasil diubah');
              }else{
  
              }
              resolve();
            }
          );
      })
  
      return promise;

    }
  }

  get f() {
    return this.editSliderForm.controls;
  }  
 
  preview(files: FileList) {
    console.log(files);
    this.fileToUpload = files.item(0);
    if (files.length === 0)
      return;
 
    var mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.message += "Only images are supported.\n";
      return;
    }else{
      this.message = "";
    }

    if (files[0].size > 100000) {
      this.message += "File size must not exceed 100 KB.\n";
      return;
    }else{
      this.message = "";
    }
 
    var reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]); 
    reader.onload = (_event) => { 
      this.imgURL = reader.result; 
    }
  }
}
