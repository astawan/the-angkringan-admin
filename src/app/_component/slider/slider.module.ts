import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SliderRoutingModule } from './slider-routing.module';
import { SliderComponent } from './slider.component';
import { AngularMaterialModule } from 'src/app/angular-material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AddSliderComponent } from './dialog/add-slider/add-slider.component';
import { ViewSliderComponent } from './dialog/view-slider/view-slider.component';


@NgModule({
  declarations: [SliderComponent, AddSliderComponent, ViewSliderComponent],
  imports: [
    CommonModule,
    SliderRoutingModule,
    AngularMaterialModule,
    FormsModule,
    ReactiveFormsModule
  ],
  entryComponents: [AddSliderComponent, ViewSliderComponent]
})
export class SliderModule { }
