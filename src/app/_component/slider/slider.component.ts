import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from 'src/app/_services/api/api.service';
import { Slider } from 'src/app/_interfaces/slider';
import { Recipe } from 'src/app/_interfaces/recipe';
import { MatTableDataSource, MatMenuTrigger, MatPaginator, MatDialogConfig, MatDialog } from '@angular/material';
import { AddSliderComponent } from './dialog/add-slider/add-slider.component';
import { ViewSliderComponent } from './dialog/view-slider/view-slider.component';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss']
})
export class SliderComponent implements OnInit {
  displayedColumns = ["name", 'image', 'status'];
  dataSlider: Slider[] = [];
  dataSource: any;
  contextMenuPosition = { x: '0px', y: '0px' };

  selectedId: string;
  selectedTitle: string;
  selectedSubtitle: string;
  selectedImage: string;
  selectedLink: string;
  selectedUrl: string;
  selectedStatus: string;
  
  @ViewChild(MatMenuTrigger, {static: false}) contextMenu: MatMenuTrigger;
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;

  constructor(
    private apiService: ApiService,
    private dialog: MatDialog,
    private http: HttpClient
  ) { }

  ngOnInit() {
    this.subscribeSliders();
  }
  
  subscribeSliders() {
    this.apiService.getBaseData(this.apiService.getAllSlidersUrl).subscribe((res: Recipe[]) => {
      console.log(res);
      this.dataSlider = res['data'];
      this.dataSource = new MatTableDataSource<Slider>(this.dataSlider);
      this.dataSource.paginator = this.paginator;
    }, err => {
    });
  }

  onContextMenu(event: MouseEvent, row) {
    event.preventDefault();
    console.log(row);
    this.selectedId = row.id;
    this.selectedTitle = row.title;
    this.selectedSubtitle = row.subtitle;
    this.selectedImage = row.image;
    this.selectedLink = row.link;
    this.selectedUrl = row.url;
    this.selectedStatus = row.status;

    this.contextMenuPosition.x = event.clientX + 'px';
    this.contextMenuPosition.y = event.clientY + 'px';
    this.contextMenu.menu.focusFirstItem('mouse');
    this.contextMenu.openMenu();
  }

  addSlider() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '700px';

    let dialogRef = this.dialog.open(AddSliderComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      this.subscribeSliders();
    });
    
  }

  viewSlider() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '700px';
    dialogConfig.data = {
      id: this.selectedId,
      title: this.selectedTitle,
      subtitle: this.selectedSubtitle,
      image: this.selectedImage,
      link: this.selectedLink,
      url: this.selectedUrl
    }

    let dialogRef = this.dialog.open(ViewSliderComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      this.subscribeSliders();
    });
  }

  disableSlider() {
    let data = {
      'user_id' : localStorage.getItem('user_id'),
      'slider_id' : this.selectedId,
    }
    let promise = new Promise((resolve, reject) => {
      let authorizedHeader = this.apiService.getAuthorizedHeader(localStorage.getItem('token'));
      this.http.post(this.apiService.changeSliderStatusUrl,data,authorizedHeader)
        .toPromise()
        .then(
          res => { // Success
            console.log(res['status']);
            if(res['status'] === true){
              this.subscribeSliders();
              console.log('Kategori berhasil di disable');
            }else{

            }
            resolve();
          }
        );
    })

    return promise;
  }

}
