import { Component, OnInit, ViewChild } from '@angular/core';
import { MatMenuTrigger, MatPaginator, MatDialog, MatTableDataSource, MatDialogConfig } from '@angular/material';
import { User } from '../../_interfaces/user'; 
import { ApiService } from 'src/app/_services/api/api.service';
import { HttpClient } from '@angular/common/http';

import { ViewUserComponent } from './dialog/view-user/view-user.component';
import { AddUserComponent } from './dialog/add-user/add-user.component';
import { DisableUserComponent } from './dialog/disable-user/disable-user.component';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  displayedColumns = ["nama", 'email', 'status'];
  dataUser: User[] = [];
  dataSource: any;
  contextMenuPosition = { x: '0px', y: '0px' };

  selectedId: string;
  selectedRoleId: string;
  selectedFullname: string;
  selectedUsername: string;
  selectedEmail: string;
  selectedPhone: string;
  selectedAddress: string;
  selectedGender: string;
  selectedAvatar: string;
  selectedStatus: string;
  selectedBanStatus: string;
  
  @ViewChild(MatMenuTrigger, {static: false}) contextMenu: MatMenuTrigger;
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;

  constructor(
    private dialog: MatDialog,
    private apiService: ApiService,
    private http: HttpClient
    ) { }

  ngOnInit() {
    this.subscribeUsers();
  }

  addUser() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;

    let dialogRef = this.dialog.open(AddUserComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      this.subscribeUsers();
    });
    
  }

  subscribeUsers() {
    this.apiService.getBaseData(this.apiService.getAllUsersUrl).subscribe((res: User[]) => {
      console.log(res);
      this.dataUser = res['data'];
      this.dataSource = new MatTableDataSource<User>(this.dataUser);
      this.dataSource.paginator = this.paginator;
    }, err => {
      console.log(err);
    });
  }

  onContextMenu(event: MouseEvent, row) {
    event.preventDefault();
    console.log(row);
    this.selectedId = row.id;
    this.selectedFullname = row.fullname;
    this.selectedUsername = row.username;
    this.selectedGender = row.gender;
    this.selectedAddress = row.address;
    this.selectedAvatar = row.avatar;
    this.selectedBanStatus = row.ban_status;
    this.selectedEmail = row.email;
    this.selectedPhone = row.phone;
    this.selectedRoleId = row.role_id;
    this.selectedStatus = row.status;

    this.contextMenuPosition.x = event.clientX + 'px';
    this.contextMenuPosition.y = event.clientY + 'px';
    this.contextMenu.menu.focusFirstItem('mouse');
    this.contextMenu.openMenu();
  }

  viewUser() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      id: this.selectedId,
      fullname: this.selectedFullname,
      username: this.selectedUsername,
      email: this.selectedEmail,
      phone: this.selectedPhone,
      gender: this.selectedGender,
      address: this.selectedAddress,
      avatar: this.selectedAvatar,
    }

    let dialogRef = this.dialog.open(ViewUserComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      this.subscribeUsers();
    });
  }

  disableUser() {
    let data = {
      'user_id' : localStorage.getItem('user_id'),
      'member_id' : this.selectedId,
    }
    let promise = new Promise((resolve, reject) => {
      let authorizedHeader = this.apiService.getAuthorizedHeader(localStorage.getItem('token'));
      this.http.post(this.apiService.disableUserUrl,data,authorizedHeader)
        .toPromise()
        .then(
          res => { // Success
            console.log(res['status']);
            if(res['status'] === true){
              this.subscribeUsers();
              console.log('User berhasil diban');
            }else{

            }
            resolve();
          }
        );
    })

    return promise;
  }

}
