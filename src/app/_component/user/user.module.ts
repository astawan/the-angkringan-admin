import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserRoutingModule } from './user-routing.module';
import { UserComponent } from './user.component';
import { AddUserComponent } from './dialog/add-user/add-user.component';
import { ViewUserComponent } from './dialog/view-user/view-user.component';
import { AngularMaterialModule } from '../../angular-material.module';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DisableUserComponent } from './dialog/disable-user/disable-user.component';


@NgModule({
  declarations: [UserComponent, AddUserComponent, ViewUserComponent, DisableUserComponent],
  imports: [
    CommonModule,
    UserRoutingModule,
    AngularMaterialModule,
    FormsModule,
    ReactiveFormsModule
  ],
  entryComponents: [AddUserComponent, ViewUserComponent, DisableUserComponent]
})
export class UserModule { }
