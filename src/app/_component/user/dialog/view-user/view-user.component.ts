import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { ApiService } from 'src/app/_services/api/api.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ViewCategoryComponent } from 'src/app/_component/category/dialog/view-category/view-category.component';

@Component({
  selector: 'app-view-user',
  templateUrl: './view-user.component.html',
  styleUrls: ['./view-user.component.scss']
})
export class ViewUserComponent implements OnInit {

  editUserForm: FormGroup;
  selectedUser = this.data.fullname;
  constructor(
    private formBuilder: FormBuilder,
    private http: HttpClient,
    private apiService: ApiService,
    private dialogRef:MatDialogRef<ViewCategoryComponent>,
    @Inject(MAT_DIALOG_DATA) private data: any
  ) { }

  ngOnInit() {
    console.log(this.data);
    this.editUserForm = this.formBuilder.group({
      fullname: [this.data.fullname, Validators.required],
      email: [this.data.email, Validators.required],
      phone: [this.data.phone, Validators.required],
      address: [this.data.address, Validators.required],
    })
  }

  close(){
    this.dialogRef.close();
  }

  saveEditCategory(){
    let data = {
      'id' : this.data.id,
      'fullname' : this.f.fullname.value,
      'email' : this.f.email.value,
      'phone' : this.f.phone.value,
      'address' : this.f.address.value
    }
    let promise = new Promise((resolve, reject) => {
      let authorizedHeader = this.apiService.getAuthorizedHeader(localStorage.getItem('token'));
      this.http.post(this.apiService.saveEditUserUrl,data,authorizedHeader)
        .toPromise()
        .then(
          res => { // Success
            console.log(res['status']);
            if(res['status'] === 'success'){
              this.dialogRef.close('User berhasil diubah');
            }else{

            }
            resolve();
          }
        );
    })

    return promise;
  }

  get f() {
    return this.editUserForm.controls;
  }

}
