import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { ApiService } from 'src/app/_services/api/api.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AddCategoryComponent } from 'src/app/_component/category/dialog/add-category/add-category.component';

@Component({
  selector: 'app-disable-user',
  templateUrl: './disable-user.component.html',
  styleUrls: ['./disable-user.component.scss']
})
export class DisableUserComponent implements OnInit {
  disableUserForm: FormGroup;
  constructor(
    private formBuilder: FormBuilder,
    private http: HttpClient,
    private apiService: ApiService,
    private dialogRef:MatDialogRef<AddCategoryComponent>,
    @Inject(MAT_DIALOG_DATA) private data: any
  ) { }

  ngOnInit() {
    this.disableUserForm = this.formBuilder.group({
      disable: ['', Validators.required],
    })
  }

  close(){
    this.dialogRef.close();
  }

  saveDisableUser(){
    let data = {
      'id' : this.data.id,
      'disable' : this.f.disable.value
    }
    let promise = new Promise((resolve, reject) => {
      let authorizedHeader = this.apiService.getAuthorizedHeader(localStorage.getItem('token'));
      this.http.post(this.apiService.disableUserUrl,data,authorizedHeader)
        .toPromise()
        .then(
          res => { // Success
            console.log(res['status']);
            if(res['status'] === 'success'){
              this.dialogRef.close('User berhasil di disable');
            }else{

            }
            resolve();
          }
        );
    })

    return promise;

  }

  get f() {
    return this.disableUserForm.controls;
  }


}
