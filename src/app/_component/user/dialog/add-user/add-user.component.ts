import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { AddCategoryComponent } from 'src/app/_component/category/dialog/add-category/add-category.component';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { ApiService } from 'src/app/_services/api/api.service';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss']
})
export class AddUserComponent implements OnInit {
  addUserForm: FormGroup;
  constructor(
    private formBuilder: FormBuilder,
    private http: HttpClient,
    private apiService: ApiService,
    private dialogRef:MatDialogRef<AddCategoryComponent>
  ) { }

  ngOnInit() {
    this.addUserForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
      fullname: ['', Validators.required],
      gender: ['', Validators.required],
      email: ['', Validators.required],
      phone: ['', Validators.required],
      address: ['', Validators.required],
    })
  }

  close(){
    this.dialogRef.close();
  }

  saveUser(){
    let data = {
      'role_id' : '1',
      'username' : this.f.username.value,
      'password' : this.f.password.value,
      'fullname' : this.f.fullname.value,
      'gender' : this.f.gender.value,
      'email' : this.f.email.value,
      'phone' : this.f.phone.value,
      'address' : this.f.address.value,
    }
    let promise = new Promise((resolve, reject) => {
      let authorizedHeader = this.apiService.getAuthorizedHeader(localStorage.getItem('token'));
      this.http.post(this.apiService.addUserUrl,data,authorizedHeader)
        .toPromise()
        .then(
          res => { // Success
            console.log(res['status']);
            if(res['status'] === 'success'){
              this.dialogRef.close('User berhasil dibuat');
            }else{

            }
            resolve();
          }
        );
    })

    return promise;
  }

  get f() {
    return this.addUserForm.controls;
  }

}
