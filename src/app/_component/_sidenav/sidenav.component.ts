import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/_services/auth/auth.service';
import { Router, NavigationEnd } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ApiService } from 'src/app/_services/api/api.service';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent implements OnInit {
  smartphone: boolean;
  routers = [
    {
      icon : 'bar_chart',
      title : 'Dashboard',
      link : '',
      active : false
    },
    {
      icon : 'apps',
      title : 'Category',
      link : 'category',
      active : false
    },
    {
      icon : 'person',
      title : 'Member',
      link : 'member',
      active : false
    },
    {
      icon : 'fastfood',
      title : 'Recipe',
      link : 'recipe',
      active : false
    },
    {
      icon : 'image',
      title : 'Slider',
      link : 'slider',
      active : false
    }
  ]
  currentRoute: number = 0;
  initialRoute: string;
  
  constructor(
    private authService: AuthService,
    private router: Router,
    private http: HttpClient,
    private apiService: ApiService) { 
      router.events.subscribe((val) => {
        if(val instanceof NavigationEnd){
          this.initialRoute = val.url.substring(val.url.lastIndexOf("/") + 1);
          (this.initialRoute === 'dashboard') ? this.initialRoute = '' : false;
          let i = this.routers.findIndex(x => x.link === this.initialRoute); 
          
          this.routers[this.currentRoute].active = false;
          if(this.routers[this.currentRoute].active == false){
            this.routers[i].active = true;
            this.currentRoute = i;
          }
        }
        
      });
    }

  ngOnInit() {
    console.log(window.innerWidth);
    if(window.innerWidth > 500){
      this.smartphone = false;
    }else{
      this.smartphone = true;
    }
    console.log(this.router.url);
  }

  logout() {
    this.authService.logout();
  }

  setActive(i){
    this.routers[this.currentRoute].active = false;
    if(this.routers[this.currentRoute].active == false){
      this.routers[i].active = true;
      this.currentRoute = i;
    }
  }

}
