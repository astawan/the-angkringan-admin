import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CategoryRoutingModule } from './category-routing.module';
import { CategoryComponent } from './category.component';
import { AddCategoryComponent } from './dialog/add-category/add-category.component';

import { AngularMaterialModule } from '../../angular-material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ViewCategoryComponent } from './dialog/view-category/view-category.component';


@NgModule({
  declarations: [CategoryComponent, AddCategoryComponent, ViewCategoryComponent],
  imports: [
    CommonModule,
    CategoryRoutingModule,
    AngularMaterialModule,
    FormsModule,
    ReactiveFormsModule
  ],
  entryComponents: [AddCategoryComponent, ViewCategoryComponent]
})
export class CategoryModule { }
