import { Component, OnInit, ViewChild } from '@angular/core';
import { AddCategoryComponent } from './dialog/add-category/add-category.component';
import { MatDialog, MatDialogConfig, MatMenuTrigger, MatPaginator, MatTableDataSource } from '@angular/material';
import { Category } from 'src/app/_interfaces/category';
import { ApiService } from 'src/app/_services/api/api.service';
import { ViewCategoryComponent } from './dialog/view-category/view-category.component';
import { HttpClient } from '@angular/common/http';
import { CategoryService } from 'src/app/_services/category/category.service';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {
  displayedColumns = ["category", 'status'];
  dataCategory: Category[] = [];
  dataSource: any;
  contextMenuPosition = { x: '0px', y: '0px' };

  selectedId: string;
  selectedCategory: string;
  selectedDescription: string;
  selectedImage: string;
  selectedStatus: string;
  
  @ViewChild(MatMenuTrigger, {static: false}) contextMenu: MatMenuTrigger;
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;

  constructor(
    private dialog: MatDialog,
    private apiService: ApiService,
    private http: HttpClient,
    private categoryService: CategoryService
    ) { 
    }

  ngOnInit() {
    this.subscribeCategories();
  }

  subscribeCategories() {
    this.apiService.getBaseData(this.apiService.getAllCategoriesUrl).subscribe((res: Category[]) => {
      this.dataCategory = res['data'];
      this.dataSource = new MatTableDataSource<Category>(this.dataCategory);
      this.dataSource.paginator = this.paginator;
    }, err => {
    });
  }

  addCategory() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '700px';

    let dialogRef = this.dialog.open(AddCategoryComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      this.subscribeCategories();
    });
    
  }

  onContextMenu(event: MouseEvent, row) {
    event.preventDefault();
    console.log(row.id);
    this.selectedId = row.id;
    this.selectedCategory = row.category_name;
    this.selectedDescription = row.description;
    this.selectedImage = row.image;
    this.selectedStatus = row.status;
    this.contextMenuPosition.x = event.clientX + 'px';
    this.contextMenuPosition.y = event.clientY + 'px';
    this.contextMenu.menu.focusFirstItem('mouse');
    this.contextMenu.openMenu();
  }

  viewCategory() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '700px';
    dialogConfig.data = {
      id: this.selectedId,
      category_name: this.selectedCategory,
      description: this.selectedDescription,
      image: this.selectedImage
    }

    let dialogRef = this.dialog.open(ViewCategoryComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      this.subscribeCategories();
    });
  }

  disableCategory() {
    let data = {
      'user_id' : localStorage.getItem('user_id'),
      'category_id' : this.selectedId,
    }
    let promise = new Promise((resolve, reject) => {
      let authorizedHeader = this.apiService.getAuthorizedHeader(localStorage.getItem('token'));
      this.http.post(this.apiService.disableCategoryUrl,data,authorizedHeader)
        .toPromise()
        .then(
          res => { // Success
            console.log(res['status']);
            if(res['status'] === true){
              this.subscribeCategories();
              console.log('Kategori berhasil di disable');
            }else{

            }
            resolve();
          }
        );
    })

    return promise;
  }

}
