import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { ApiService } from 'src/app/_services/api/api.service';
import { MatDialogRef } from '@angular/material';
import { AuthService } from 'src/app/_services/auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-category',
  templateUrl: './add-category.component.html',
  styleUrls: ['./add-category.component.scss']
})
export class AddCategoryComponent implements OnInit {
  addCategoryForm: FormGroup;

  public imagePath;
  imgURL: any = this.apiService.imagesno;
  public message: string;
  fileToUpload: File = null;

  constructor(
    private formBuilder: FormBuilder,
    private http: HttpClient,
    private apiService: ApiService,
    private authService: AuthService,
    private dialogRef:MatDialogRef<AddCategoryComponent>,
    private router: Router
  ) { }

  ngOnInit() {
    this.addCategoryForm = this.formBuilder.group({
      category_name: ['', Validators.required],
      description: ['', Validators.required],
      image: [''],
    })
  }

  close(){
    this.dialogRef.close();
  }

  saveCategory(){
    const formData: FormData =  new FormData();
    formData.append('file', this.fileToUpload, this.fileToUpload.name);
    formData.append('user_id', localStorage.getItem('user_id'));
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'multipart/form-data');
    headers.append('Accept', 'application/json');
    let promise_upload = new Promise((resolve, reject) => {
      this.http.post(this.apiService.uploadCategoryImageUrl,formData,{headers: headers})
        .toPromise()
        .then(
          res => { // Success
            console.log(res);
            if(res['status'] === true){
              let data = {
                'user_id' : localStorage.getItem('user_id'),
                'category_name' : this.f.category_name.value,
                'description' : this.f.description.value,
                'image' : `${this.apiService.upload_root}category/${res['data'].image}`
              }
              let promise = new Promise((resolve, reject) => {
                let authorizedHeader = this.apiService.getAuthorizedHeader(localStorage.getItem('token'));
                this.http.post(this.apiService.addCategoryUrl,data,authorizedHeader)
                  .toPromise()
                  .then((res) => { // Success
                      console.log(res);
                      if(res['status'] === true){
                        this.dialogRef.close('Kategori berhasil disimpan');
                      }
                      resolve();
                    }
                  )
              })
          
              return promise;
            }else{

            }
            resolve();
          }
        );

    })
    
    return promise_upload;
  }

  get f() {
    return this.addCategoryForm.controls;
  }
 
  preview(files: FileList) {
    console.log(files);
    this.fileToUpload = files.item(0);
    if (files.length === 0)
      return;
 
    var mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.message += "Only images are supported.\n";
      return;
    }else{
      this.message = "";
    }

    if (files[0].size > 100000) {
      this.message += "File size must not exceed 100 KB.\n";
      return;
    }else{
      this.message = "";
    }
 
    var reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]); 
    reader.onload = (_event) => { 
      this.imgURL = reader.result; 
    }
  }

}
