import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../_services/auth/auth.service';
import { Login } from '../../_interfaces/login';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserService } from 'src/app/_services/User/user.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ApiService } from 'src/app/_services/api/api.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  message: string;
  errorLogin = false;

  constructor(
    private router: Router,
    private authService: AuthService,
    private userService: UserService,
    private apiService: ApiService,
    private formBuilder: FormBuilder,
    private http: HttpClient,
  ) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    })
    // this.authService.logout();
  }

  login(){
    this.authService.login(this.f.username.value, this.f.password.value);
  }

  register(){
    this.userService.register();
  }

  get f() {
    return this.loginForm.controls;
  }
}
