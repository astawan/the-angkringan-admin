import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/_services/api/api.service';
import { Dashboard } from 'src/app/_interfaces/dashboard';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  category: string;
  member: string;
  recipe: string;

  constructor(
    private apiService: ApiService
  ) { }

  ngOnInit() {
    this.subscribeDashboard();
  }
  
  subscribeDashboard() {
    this.apiService.getBaseData(this.apiService.getDashboardDataUrl).subscribe((res: Dashboard[]) => {
      console.log(res['data'].category);
      this.category = res['data'].category;
      this.member = res['data'].member;
      this.recipe = res['data'].recipe;
    }, err => {
    });
  }

}
