import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './_services/auth/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'theangkringanadmin';
  loginState: any;
  constructor(
    private router: Router,
    private authService: AuthService
  ) { 
    localStorage.clear();
    this.loginState = localStorage.getItem('isLoggedIn');
    if(!this.loginState || this.loginState == 'false'){
      this.router.navigate(['/login']);
    }
    
    this.authService.isLoggedIn.subscribe(value => {
      this.loginState = value;
    })
  }

}
