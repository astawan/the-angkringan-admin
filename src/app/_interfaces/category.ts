export interface Category {
    category_name: string;
    description: string;
    image: string;
    status: string;
}
