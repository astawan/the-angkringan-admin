export interface Recipe {
    recipe_name:string,
    recipe_category:string,
    description:string,
    image:string,
    gallery:string,
    video:string,
    ingredients:string,
    cookmethd:string,
    notes:string,
    province:string,
    city:string,
    status:string
}
