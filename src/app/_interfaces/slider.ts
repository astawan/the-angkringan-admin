export interface Slider {
    title:string,
    subtitle:string,
    image:string,
    link:string,
    url:string,
    status:string
}
