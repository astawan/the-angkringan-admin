export interface User {
    roleId: string;
    fullname: string;
    username: string;
    email: string;
    phone: string;
    address: string;
    gender: string;
    avatar: string;
    status: string;
    banStatus: string;
}
