import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { ApiService } from '../api/api.service';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Category } from 'src/app/_interfaces/category';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  constructor(
    private http: HttpClient,
    private apiService: ApiService,
  ) { }
  
  getAllCategories(): Observable<any> {
    return this.http.get(this.apiService.getAllCategoriesUrl)
      .pipe(
        map( (res: Category[]) => {
          return res;
        }), catchError (err => {
          return err;
        })
      )
  }
}
