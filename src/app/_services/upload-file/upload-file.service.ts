import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { ApiService } from '../api/api.service';

@Injectable({
  providedIn: 'root'
})
export class UploadFileService {

  constructor(
    private http: HttpClient,
    private apiService: ApiService
  ) { }

  postFileImageCategory(fileToUpload: File): Observable<any> {
    const formData: FormData = new FormData();
    formData.append('file', fileToUpload, fileToUpload.name);
    return this.http.post(this.apiService.uploadCategoryImageUrl, formData, this.apiService.getAuthorizedHeader(localStorage.getItem('token')) )
      .pipe(
        map(() => { return true; }),
        catchError((e) => { return e; })
      )
  }
}
