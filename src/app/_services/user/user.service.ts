import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Subject } from 'rxjs/internal/Subject';
import { ApiService } from '../api/api.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  loginState = false;
  loginStateChange: Subject<boolean> = new Subject<boolean>();
  constructor(
    private http: HttpClient,
    private apiService: ApiService
  ) {
    this.loginStateChange.subscribe((value) => {
      this.loginState = value
    });
  }

  toggleLoginState() {
    this.loginStateChange.next(!this.loginState);
  }

  register(){    
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': '',
      })
    };
    let promise = new Promise((resolve, reject) => {
      this.http.post(this.apiService.addUserUrl,
        {
          'fullname' : 'Kadek Darmaastawan',
          'email' : 'k.darmaastawan@gmail.com',
          'phone' : '081732076864',
          'address' : 'Denpasar',
          'gender' : 'l',
          'password' : '123456'
        },
        httpOptions)
        .toPromise()
        .then(
          res => { // Success
            console.log(res);
            resolve();
          }
        );
    })

    return promise;
  }
}
