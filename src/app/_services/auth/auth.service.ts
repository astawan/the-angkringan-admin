import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { ApiService } from '../api/api.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  isLoggedIn = new Subject();
  constructor(
    private http: HttpClient,
    private apiService: ApiService,
    private router: Router
  ) { }

  set loggedIn(value) {
    this.isLoggedIn.next(value);
    localStorage.setItem('isLoggedIn', value);
  }

  get loggedIn() {
    return localStorage.getItem('isLoggedIn');
  }  

  login(username, password){
    console.log(this.isLoggedIn);
    let data = {
      'email' : username,
      'password' : password
    }
    let promise = new Promise((resolve, reject) => {
      this.http.post(this.apiService.loginUrl,data,this.apiService.getUnauthorizedHeader())
        .toPromise()
        .then(
          res => { // Success
            console.log(res);
            if(res['status'] === true && res['status_code'] === 200){
              localStorage.setItem('isLoggedIn', 'true');
              localStorage.setItem('token', res['data'].token);  
              localStorage.setItem('email', res['data'].email);  
              localStorage.setItem('name', res['data'].name);  
              localStorage.setItem('user_id', res['data'].user_id); 
              if(localStorage.getItem('token') != ''){
                console.log(localStorage.getItem('token'));
                this.router.navigate(['/dashboard']);
              }
            }else{
            }
            resolve();
          }
        );
    })

    return promise;
  }
  
  logout(){
    let data = {
      'user_id' : localStorage.getItem('user_id'),
    }
    let promise = new Promise((resolve, reject) => {
      this.http.post(this.apiService.logoutUrl,data,this.apiService.getAuthorizedHeader(localStorage.getItem('token')))
        .toPromise()
        .then(
          res => { // Success
            console.log(res);
            if(res['status'] === true && res['status_code'] === 200){
              localStorage.setItem('isLoggedIn', 'false');
              localStorage.removeItem('token');  
              localStorage.removeItem('email');  
              localStorage.removeItem('name');  
              localStorage.removeItem('user_id'); 
              if(!localStorage.getItem('token')){
                this.router.navigate(['/login']);
              }
            }else{
            }
            resolve();
          }
        );
    })

    return promise;
  }
}
