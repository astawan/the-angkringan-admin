import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Category } from 'src/app/_interfaces/category';
import { User } from 'src/app/_interfaces/user';
import { Recipe } from 'src/app/_interfaces/recipe';
import { Slider } from 'src/app/_interfaces/slider';
import { Dashboard } from 'src/app/_interfaces/dashboard';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  upload_root: string;
  imagesno: string;
  root: string;
  // environment = 'development';
  environment = 'production';
  loginUrl: string;
  logoutUrl: string;

  getDashboardDataUrl: string;

  getAllCategoriesUrl: string;
  addCategoryUrl: string;
  saveEditCategoryUrl: string;
  disableCategoryUrl: string;
  uploadCategoryImageUrl: string;

  getAllUsersUrl: string;
  addUserUrl: string;
  saveEditUserUrl: string;
  disableUserUrl: string;

  getAllRecipesUrl: string;
  changeRecipeStatusUrl: string;

  getAllSlidersUrl: string;
  addSliderUrl: string;
  saveEditSliderUrl: string;
  uploadSliderImageUrl: string;
  changeSliderStatusUrl: string;
  
  private httpOptionsUnauthorized = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': '',
    })
  };

  constructor(
    private http: HttpClient,
  ) {
    this.upload_root = 'https://theangkringan.harmonydshine.web.id/assets/apiv1/';
    this.imagesno = 'https://theangkringan.harmonydshine.web.id/assets/apiv1/recipe/imagesno.jpeg';

    if(this.environment == 'development'){
      this.root = 'http://127.0.0.1/the-angkringan-db/public/api/'
    }else{
      this.root = 'https://theangkringan.harmonydshine.web.id/api/v1/'
    }
    
    this.loginUrl = `${this.root}admin/login`;
    this.logoutUrl = `${this.root}admin/logout`;

    this.getDashboardDataUrl = `${this.root}admin/dashboard`;

    this.getAllCategoriesUrl = `${this.root}admin/categories`;
    this.addCategoryUrl = `${this.root}categories/add`;
    this.saveEditCategoryUrl = `${this.root}admin/categories/edit`;
    this.disableCategoryUrl = `${this.root}admin/categories/change_status`;
    this.uploadCategoryImageUrl = `${this.root}admin/categories/upload_image`;

    this.getAllUsersUrl = `${this.root}admin/members`;
    this.disableUserUrl = `${this.root}admin/members/change_status`;
    this.addUserUrl = `${this.root}admin/register`;
    this.saveEditUserUrl = `${this.root}save_edit_user`;

    this.getAllRecipesUrl = `${this.root}admin/recipes`;
    this.changeRecipeStatusUrl = `${this.root}admin/recipes/change_status`;

    this.getAllSlidersUrl = `${this.root}admin/sliders`;
    this.addSliderUrl = `${this.root}sliders/add`;
    this.saveEditSliderUrl = `${this.root}admin/sliders/edit`;
    this.changeSliderStatusUrl = `${this.root}admin/sliders/change_status`;
    this.uploadSliderImageUrl = `${this.root}admin/sliders/upload_image`;
  }

  getUnauthorizedHeader(){
    return this.httpOptionsUnauthorized;
  }

  getAuthorizedHeader(token){
    return {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `${token}`,
      })
    };
  }

  getBaseData(url): Observable<any>{
    return this.http.get(url)
      .pipe(
        map( (res) => {
          return res;
        }), catchError (err => {
          return err;
        })
      )
  }
}
